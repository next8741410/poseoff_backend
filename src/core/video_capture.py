import cv2
import threading
import queue

class VideoException(Exception):
    """
    Raised When unable to open camera feed
    """
    pass

# bufferless VideoCapture
class VideoCapture:
    def __init__(self, url):
        self.cap = cv2.VideoCapture(url)
        self.q = queue.Queue()
        self.lock = threading.Lock()
        self.running = True  # Flag to indicate if the thread should keep running
        self.t = threading.Thread(target=self._reader)
        self.t.daemon = True
        self.t.start()
        self.state = False

    def _reader(self):
        while self.running:
            if not self.cap.isOpened():
                self.running = False
                continue
            self.running = True
            ret, frame = self.cap.read()
            if not ret:
                continue
            if not self.q.empty():
                try:
                    self.q.get_nowait()  # Discard previous frame
                except queue.Empty:
                    pass
            self.q.put(frame)
            self.state = ret

    def read(self):
        if not self.running:
            raise VideoException("Video Stream is no Running")
        return self.state, self.q.get()

    def stop(self):
        self.running = False
        self.t.join() 