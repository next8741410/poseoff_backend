from fastapi import APIRouter
from src.endpoints import utils

router = APIRouter()

router.include_router(utils.router)