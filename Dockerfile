FROM python:3.8-slim
 
RUN apt-get update
 
WORKDIR /cache
 
RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
    build-essential  \
    libkrb5-dev \
    libsm6 \
    libxext6 \
    ffmpeg \
    libfontconfig1 \
    libxrender1  \
    libgl1-mesa-glx \
    libreoffice && \
    rm -rf /var/lib/apt/lists/*
 
 
COPY requirements.txt requirements.txt 
RUN pip install -r requirements.txt
 
COPY . /cache
 
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8082"]