from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, TIMESTAMP
from sqlalchemy.orm import relationship
from datetime import datetime
from .Database import Base, engine



class PoseoffDetails(Base):
    __tablename__ = 'poseoff_details'

    _id = Column(Integer, primary_key=True, unique=True, index=True)
    username  = Column(String(100))
    platform = Column(String(50))
    pose_count = Column(Integer)
    p1_score = Column(String(50))
    p2_score = Column(String(50))
    p3_score = Column(String(50))
    p4_score = Column(String(50))
    p5_score = Column(String(50))
    p6_score = Column(String(50))
    p7_score = Column(String(50))
    p8_score = Column(String(50))
    p9_score = Column(String(50))
    p10_score = Column(String(50))
    final_score = Column(Integer)
    img_link = Column(String(100))
    timestamp = Column(TIMESTAMP(timezone=True), default=datetime.now)



class SettingDetails(Base):
    __tablename__ = 'setting_details'

    _id = Column(Integer, primary_key=True, unique=True, index=True)
    game_mode = Column(String(255))
    cam_mode = Column(String(255))
    cam_url = Column(String(255))

Base.metadata.create_all(engine)

