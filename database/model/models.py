from pydantic import BaseModel
from config import Pathconfig, Database
from typing import Union



class PoseOff(BaseModel):
    tx_id: int
    username: Union[str, None] = None
    platform: str
    p1_score: str
    p2_score: str
    p3_score: str
    p4_score: str
    p5_score: str
    p6_score: str
    p7_score: str
    p8_score: str
    p9_score: str
    p10_score: str
    final_score: int
    


class Settings(BaseModel):
    game_mode: Union[str, None] = None
    cam_mode: Union[str, None] = None
    cam_url: Union[str, None] = None
    

