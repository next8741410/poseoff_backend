import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

class Database:
    db_url = os.getenv("DB_URL", "mysql+mysqlconnector://root:audaxlabs@192.168.150.251:8081/EdgePoc")

  
    
class Pathconfig:
    BasePath = os.getenv("BASEPATH")
    mount_point = os.getenv("MOUNT_POINT")
    skip_count=int(os.getenv("frame_skip_count","15"))
    rtsp_url = os.getenv("RTSP_URL")

class User:
    first_user = "Anonymous1"
    user = "Anonymous"
    count = 1
    
    
